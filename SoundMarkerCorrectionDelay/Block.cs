﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoundMarkerCorrection
{
    class Block
    {
        public List<Trial> Trials = new List<Trial>();
        int StartPosition = 0;
        public string TrialType;
        public int oldCount, newCount;
        public int Blockmarker = 0;




        public int Count { get { return Trials.Count; } }



        int GetType(Trial t)
        {
            if (new List<int>() { 9, 10 }.Contains(Blockmarker))
            {
                if (new List<int>() { 140, 141 }.Contains(t.Marker)) return 6000;
                if (Experiment.Proband == 1) if (new List<int>() { 150, 160 }.Contains(t.Marker)) return 3000;
                if (Experiment.Proband == 2) if (new List<int>() { 150, 160 }.Contains(t.Marker)) return 4000;
            }
            if (new List<int>() { 11, 12 }.Contains(Blockmarker))
            {
                if (new List<int>() { 140, 141 }.Contains(t.Marker)) return 7000;
                if (Experiment.Proband == 1) if (new List<int>() { 150, 160 }.Contains(t.Marker)) return 4000;
                if (Experiment.Proband == 2) if (new List<int>() { 150, 160 }.Contains(t.Marker)) return 3000;
            }
            if (new List<int>() { 13, 14 }.Contains(Blockmarker))
            {
                if (Experiment.Proband == 1) if (new List<int>() { 150, 160 }.Contains(t.Marker)) return 3000;
                if (Experiment.Proband == 2) if (new List<int>() { 150, 160 }.Contains(t.Marker)) return 4000;
            }
            if (new List<int>() { 15, 16 }.Contains(Blockmarker))
            {
                if (Experiment.Proband == 1) if (new List<int>() { 150, 160 }.Contains(t.Marker)) return 4000;
                if (Experiment.Proband == 2) if (new List<int>() { 150, 160 }.Contains(t.Marker)) return 3000;
            }
            if (new List<int>() { 17 }.Contains(Blockmarker))
            {
                if (new List<int>() { 140, 141 }.Contains(t.Marker)) return 9000;
            }
            if (new List<int>() { 18 }.Contains(Blockmarker))
            {
                if (new List<int>() { 140, 141 }.Contains(t.Marker)) return 8000;
                if (new List<int>() { 170, 171, 180, 181 }.Contains(t.Marker)) return 5000;
            }
            if (new List<int>() { 19 }.Contains(Blockmarker))
            {
                if (new List<int>() { 170, 171, 180, 181 }.Contains(t.Marker)) return 5000;
            }
            return 0;
        }

        int GetSubtype(Trial t)
        {
            if (new List<int>() { 9, 10, 11, 12 }.Contains(Blockmarker))
            {
                if (new List<int>() { 140, 141 }.Contains(t.MarkerValue())) return 0;
                if (t.MarkerValue() == 150) return 100;
                if (t.MarkerValue() == 160) return 300;
            }

            if (new List<int>() { 13, 14, 15, 16 }.Contains(Blockmarker))
            {
                if (new List<int>() { 140, 141 }.Contains(t.MarkerValue())) return 0;
                if (t.MarkerValue() == 150) return 200;
                if (t.MarkerValue() == 160) return 400;
            }


            if (new List<int>() { 18 }.Contains(Blockmarker))
            {
                if (new List<int>() { 170, 171 }.Contains(t.Marker)) return 100;
                if (new List<int>() { 180, 181 }.Contains(t.Marker)) return 300;
            }

            if (new List<int>() { 19 }.Contains(Blockmarker))
            {
                if (new List<int>() { 170, 171 }.Contains(t.Marker)) return 200;
                if (new List<int>() { 180, 181 }.Contains(t.Marker)) return 400;
            }
            return 0;
        }

        int GetAccuracy(Trial t, int marker = 0)
        {
            if (t.Onset >= 0 && t.Onset < 800)
                return 40;
            if (t.Onset < 1800) return 10;
            if (t.Onset > 10000) return 50;
            if (t.Onset > 3000) return 20;
            return 0; //Correct
        }

        int GetHand(Trial t)
        {
            if (t.Marker == 140) return 0;
            if (t.Marker == 141) return 1;
            if (new List<int>() { 150, 160 }.Contains(t.Marker))
            {
                if (Blockmarker % 2 == 0) return 1; //even
                if (Blockmarker % 2 == 1) return 0; //odd
            }
            return 0;
        }


        public List<KeyValuePair<string, int>> Stats, NewStats = new List<KeyValuePair<string, int>>();


        public Block(List<Trial> trials, Range r)
        {
            StartPosition = trials[r.Start].Position;
            Trials = trials.GetRange(r.Start, r.Length);
            Trials.ForEach(x => { x.hasBlock = true; });
            Trials.RemoveAll(x => x.Type != "Stimulus");
            Blockmarker = r.Blockmarker;

            oldCount = Trials.Count;
            Stats = (from t in Trials orderby t.Description group t by t.Description into g select new KeyValuePair<string, int>(g.Key, g.Count())).ToList();
            if (Trials.Count < 5) { TrialType = "none"; return; }


            Trial lastDown = null;
            int onset = -1;
            foreach (Trial t in Trials)
            {
                if (t.Marker == 150 || t.Marker == 170 || t.Marker == 171 || ((t.Marker == 140 || t.Marker == 141) && (Blockmarker == 17)))
                {
                    if (lastDown == null)
                    {
                        onset = -1;
                    }
                    else
                    {
                        onset = t.Position - lastDown.Position;
                    }
                    lastDown = t;
                }
                t.Onset = onset;
            }
            List<Trial> newTrials = new List<Trial>();


            foreach (Trial t in Trials)
            {
                if (t.MarkerValue() > 20 && t.Onset > 0)
                {
                    t.NewMarker = GetType(t) + GetSubtype(t) + GetAccuracy(t) + GetHand(t);
                    t.SetDescription(t.NewMarker.ToString());
                }
            }

            int pos = 0;

            List<Trial> notones = Trials.Where(x => x.Marker != 140 && x.Marker != 141).ToList();

            while (pos < notones.Count && (pos = notones.FindIndex(pos, t => { return (t.Marker == 150 || t.Marker == 170 || t.Marker == 171 || (Blockmarker == 17)) && GetAccuracy(t) == 40; })) > -1)
            {
                if (pos > 1)
                {
                    Trial lastUp = notones[pos - 1];
                    Trial lastDwn = notones[pos - 2];

                    int newlastUp = GetType(lastUp) + GetSubtype(lastUp) + 30 + GetHand(lastUp);
                    lastUp.SetDescription(newlastUp.ToString());
                    int newlastDwn = GetType(lastDwn) + GetSubtype(lastDwn) + 30 + GetHand(lastDwn);
                    lastDwn.SetDescription(newlastDwn.ToString());

                }
                if (pos < notones.Count - 2)
                {
                    Trial Up = notones[pos + 1];
                    int newUp = GetType(Up) + GetSubtype(Up) + GetAccuracy(Up) + GetHand(Up);
                    Up.SetDescription(newUp.ToString());
                }
                pos += 2;
            }

            Trials.RemoveAll(x => Messung.ToBeDeleted.Contains(x.Description));
            //Trials.RemoveAll(x => x.NewMarker == 0);
            newCount = Trials.Count;

            trials[r.Start - 1].line.Append((string.Format(";BEGIN {0} Lines: {1}", TrialType, newCount)));
            trials[r.Start - 1].hasBlock = true;

            NewStats = (from t in Trials where t.newDescription != "" orderby t.newDescription group t by t.newDescription into g select new KeyValuePair<string, int>(g.Key, g.Count())).ToList();
        }

        public override string ToString()
        {
            return string.Format("{0} {1}", TrialType, Trials.Count);
        }
    }
}
