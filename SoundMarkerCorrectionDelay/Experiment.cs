﻿using System.Collections.Generic;
using System.IO;

namespace SoundMarkerCorrection
{
    class Experiment
    {
        public List<Messung> Messungen = new List<Messung>(15);

        static DirectoryInfo basedir;
        FileInfo[] logs;
        public static int Proband = 1;

        public Experiment()
        {

        }

        public void Open(DirectoryInfo dir, int proband)
        {
            Proband = proband;
            Messungen.Clear();
            Program.Form.ClearLog();
            basedir = dir;
            logs = basedir.GetFiles("*.vmrk");
            foreach (FileInfo log in logs)
            {
                Messungen.Add(new Messung(log));
            }

            WriteResult();
        }


        public void WriteResult()
        {
            FileInfo resultFile = new FileInfo(basedir.FullName + "result.csv");
            if (resultFile.Exists) resultFile.Delete();
            StreamWriter writer = new StreamWriter(resultFile.FullName);
            writer.Close();
        }

        public void Save() {
            foreach (Messung m in Messungen) m.Write();
        }

    }
}
