﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.IO;
using System.Text;

namespace SoundMarkerCorrection
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Experiment exp = new Experiment();

        ListViewGroup group = null;

        public void ClearLog() {
            listView1.Items.Clear();
            listView1.Groups.Clear();
        }

        public void Log(string file,  string marker, int lines, string explanation )
        {
            if (group != null)
            {
                if (group.Header != file)
                    listView1.Groups.Add(group = new ListViewGroup(file));
            }
            else
                listView1.Groups.Add(group = new ListViewGroup(file));

            ListViewItem item = new ListViewItem(new string[] {  marker, lines.ToString(), explanation }, group);
            listView1.Items.Add(item);
        }

        private void resultsListView_KeyUp(object sender, KeyEventArgs e)
        {
            if (sender != listView1) return;

            if (e.Control && e.KeyCode == Keys.C)
                CopySelectedValuesToClipboard();
        }

        private void CopySelectedValuesToClipboard()
        {
            var builder = new StringBuilder("");
            //builder.AppendLine(listView1.SelectedItems[0].Group)
            foreach (ListViewItem item in listView1.Items)
            {
                builder.Append(item.Group.Header);
                foreach (ListViewItem.ListViewSubItem s in item.SubItems) {
                    builder.Append('\t'); builder.Append(s.Text);
                }
                builder.AppendLine();
            }
            builder.AppendLine();
            Clipboard.SetText(builder.ToString());
        }


        private void toolStripContainer1_ContentPanel_Paint(object sender, PaintEventArgs e)
        {

            Matrix m = new Matrix();
            m.Translate(50, (this.Height - 50) / 2);
            m.Scale((this.Width) / 75, -(this.Height - 100) / 8);

            GraphicsPath threshPath = new GraphicsPath();
            Pen pen1 = new Pen(Color.Red);
            pen1.DashStyle = DashStyle.DashDot;
            threshPath.AddLine(0, 1, 75, 1); threshPath.CloseFigure();
            threshPath.AddLine(0, -1, 75, -1);
            threshPath.Transform(m);
            e.Graphics.DrawPath(pen1, threshPath);

            GraphicsPath medPath = new GraphicsPath();
            Pen pen2 = new Pen(Color.LightSalmon);
            pen2.DashStyle = DashStyle.DashDot;
            medPath.AddLine(0, 2, 75, 2); medPath.CloseFigure();
            medPath.AddLine(0, -2, 75, -2);
            medPath.Transform(m);
            e.Graphics.DrawPath(pen2, medPath);

            GraphicsPath axePath = new GraphicsPath();
            axePath.AddLine(0, 0, 75, 0); axePath.CloseFigure();
            axePath.AddLine(0, -4, 0, 4);
            axePath.Transform(m);
            e.Graphics.DrawPath(Pens.DarkGray, axePath);

            GraphicsPath xPath = new GraphicsPath();
            Pen pen3 = new Pen(Color.LightGray);
            pen2.DashStyle = DashStyle.DashDot;
            xPath.AddLine(9, -4, 9, 4); xPath.CloseFigure();
            xPath.AddLine(19, -4, 19, 4); xPath.CloseFigure();
            xPath.AddLine(29, -4, 29, 4); xPath.CloseFigure();
            xPath.AddLine(39, -4, 39, 4); xPath.CloseFigure();
            xPath.AddLine(49, -4, 49, 4); xPath.CloseFigure();
            xPath.AddLine(59, -4, 59, 4); xPath.CloseFigure();
            xPath.AddLine(69, -4, 69, 4); xPath.CloseFigure();
            xPath.AddLine(75, -4, 75, 4); xPath.CloseFigure();
            xPath.Transform(m);
            e.Graphics.DrawPath(pen3, xPath);

            GraphicsPath textPath = new GraphicsPath();
            textPath.AddString("3.6s", FontFamily.GenericMonospace, 0, 12, new Point(10, (Height - 100) * 2 / 8 + 16), StringFormat.GenericTypographic); axePath.CloseFigure();
            textPath.AddString("3.0s", FontFamily.GenericMonospace, 0, 12, new Point(10, (Height - 100) * 3 / 8 + 16), StringFormat.GenericTypographic);
            textPath.AddString("2.4s", FontFamily.GenericMonospace, 0, 12, new Point(10, (Height - 100) * 4 / 8 + 16), StringFormat.GenericTypographic);
            textPath.AddString("1.8s", FontFamily.GenericMonospace, 0, 12, new Point(10, (Height - 100) * 5 / 8 + 16), StringFormat.GenericTypographic);
            textPath.AddString("1.2s", FontFamily.GenericMonospace, 0, 12, new Point(10, (Height - 100) * 6 / 8 + 16), StringFormat.GenericTypographic);
            e.Graphics.DrawPath(Pens.DarkGray, textPath);




            PointF[] tpoints = new PointF[] { new PointF(), new PointF() };
            //Program.exp.messungen[index].Sequenz[block].subblocks[0].points.ToArray();
            m.TransformPoints(tpoints);
            e.Graphics.DrawLines(Pens.Green, tpoints);

            //tpoints = Program.exp.messungen[index].Sequenz[block].subblocks[1].points.ToArray();
            m.TransformPoints(tpoints);
            e.Graphics.DrawLines(Pens.Blue, tpoints);

        }


        private void toolStripButtonOpen_Click(object sender, EventArgs e)
        {
            int Proband = 1;
            //folderBrowserDialog1.SelectedPath = @"C:\Users\Juergen\Desktop\Sophie\SoundMarkerCorrectionDelay\Markerfiles_simEEG";
            folderBrowserDialog1.ShowDialog();
            if (folderBrowserDialog1.SelectedPath != "")
            {
                DirectoryInfo dir = new DirectoryInfo(folderBrowserDialog1.SelectedPath);
                FileInfo[] files = dir.GetFiles("*.vmrk");
                if (files.Length == 0)  MessageBox.Show("Keine Markerfiles gefunden");
                string item = (string)toolStripComboBox1.SelectedItem;
                if (item == "Zweiter Proband") Proband = 2;
                exp.Open(dir, Proband);
            }
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void toolStripButtonSelect_Click(object sender, EventArgs e)
        {
            ListView listView = Program.Form.listView1;
            for (int i = 0; i < listView.Items.Count; i++)
            {
                listView.Items[i].Selected = true;
            }
        }

        private void toolStripButtonCopy_Click(object sender, EventArgs e)
        {
            CopySelectedValuesToClipboard();
        }

        private void toolStripButtonSave_Click(object sender, EventArgs e)
        {
            exp.Save(); 
        }

        private void toolStripComboBox1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            toolStripComboBox1.SelectedIndex = 0;
        }
    }
}
