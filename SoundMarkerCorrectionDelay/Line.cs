﻿using System.Collections.Generic;

namespace SoundMarkerCorrection
{
    class Line
    {
        string buffer;
        public string Marker= "";
        public int Nr=0;

        Dictionary<string, int> header = new Dictionary<string, int>
        {
            {"Type", 0},
            {"Description", 1},
            {"Position", 2},
            {"Size", 3}
        };

        string[] values;

        public bool IsMarker { get { return Marker != ""; } }

        public Line(string s)
        {
            buffer = s;
            if (s.StartsWith("Mk"))
            {
                string[] parts = s.Split('=');
                Marker = parts[0];
                Nr = int.Parse(Marker.Substring(2));

                values = parts[1].Split(',');
            }
        }

        public void Replace(string old, string newstring)
        {
            buffer = buffer.Replace(old, newstring);
        }

        public void Append(string s)
        {
        buffer += s;
        }

        public Line(Line l)
        {
            buffer = (string)l.buffer.Clone();
        }

        public string sValue(string key)
        {
            return values[header[key]];
        }

        public int iValue(string key)
        {
            return int.Parse(values[header[key]]);
        }

        public override string ToString()
        {
            return buffer;
        }
    }
}
