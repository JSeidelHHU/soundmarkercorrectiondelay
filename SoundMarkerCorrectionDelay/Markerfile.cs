﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SoundMarkerCorrection
{
    class MarkerFile
    {
        FileInfo file;

        Dictionary<string, int> columns = new Dictionary<string, int>();
        List<string> data = new List<string>();
        public List<Trial> Trials = new List<Trial>();

        public MarkerFile(FileInfo file)
        {
            this.file = file;
            read();
        }

        void read()
        {
            string[] lines;
            if (!file.Exists) throw new FileNotFoundException("Markerfile not found", file.Name);
            StreamReader reader = file.OpenText(); try
            {
                string buffer = reader.ReadToEnd();
                lines = buffer.Split(new string[] { "\r\n" }, StringSplitOptions.None);
                if (lines.Length == 1) lines = buffer.Split(new string[] { "\n" }, StringSplitOptions.None);
                for (int i = 0; i< lines.Length;i++)
                {
                    Trials.Add(new Trial(new Line(lines[i]), i));
                }
            }
            finally
            {
                reader.Close();
            }
        }

        public void Write()
        {
            DirectoryInfo newDir = new DirectoryInfo(Path.Combine(file.DirectoryName,"new"));
            if (!newDir.Exists) newDir.Create();
            
            FileInfo resultFile = new FileInfo( Path.Combine(newDir.FullName , file.Name));
            if (resultFile.Exists) resultFile.Delete();
            StreamWriter writer = new StreamWriter(resultFile.FullName);
            /*
            List<Trial> Header = Trials.GetRange(0, 13);

            foreach (Trial t in Header)
            {
                writer.WriteLine(t.line.ToString());
            }
            */
            foreach (Trial t in Trials)
            {
                //if (t.hasBlock)
                if(t.newLine == null) writer.WriteLine(t.line.ToString());
                else
                writer.WriteLine(t.newLine.ToString());
            }

            writer.Close();
        }

        public Range GetRange(int start)
        {
            int b1 = Trials.FindIndex(start, x => x.isBlockmarker);
            if (b1 < 0) return null;
            int b2 = Trials.FindIndex(b1 + 1, x => x.isBlockmarker);
            if (Trials[b1].MarkerValue() == Trials[b2].MarkerValue())
                return new Range(b1, b2, Trials[b1].MarkerValue());
            else throw new Exception("Invalid markerfile");
        }
    }
}
