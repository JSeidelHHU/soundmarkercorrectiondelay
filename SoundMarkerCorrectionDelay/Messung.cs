﻿using System;
using System.IO;
using System.Collections.Generic;

namespace SoundMarkerCorrection
{
    class Messung
    {
        public readonly string Proband;
        public readonly MarkerFile Log;
        public readonly List<Block> Blocks = new List<Block>();


        public static List<string> ToBeDeleted = new List<string>   {  };

        Dictionary<string, string> Glossary = new Dictionary<string, string> {
            {"S  1", "Met 1 Block Marker R"},
            {"S  2", "Met 1 Block Marker L"},
            {"S  3", "Met 2 Block Marker R"},
            {"S  4", "Met 2 Block Marker L"},
            {"S  5", "Tra 1 Block Marker R"},
            {"S  6", "Tra 1 Block Marker L"},
            {"S  7", "Tra 2 Block Marker R"},
            {"S  8", "Tra 2 Block Marker L"},
            {"S  9", "AMC 1 Block Marker R"},
            {"S 10", "AMC 1 Block Marker L"},
            {"S 11", "AMC 2 Block Marker R"},
            {"S 12", "AMC 2 Block Marker L"},
            {"S 13", "MOC 1 Block Marker R"},
            {"S 14", "MOC 1 Block Marker L"},
            {"S 15", "MOC 2 Block Marker R"},
            {"S 16", "MOC 2 Block Marker L"},
            {"S 17", "AOC Block Marker"},
            {"S 18", "AVC Block Marker"},
            {"S 19", "VOC Block Marker"},
            {"S110","AMC/AOC/AVC Ton Referenz  R"},
            {"S111","AMC/AOC/AVC Ton Referenz  L"},
            {"S130","AVC/VOC Cue Referenz  R"},
            {"S131","AVC/VOC Cue Referenz  L"},
            {"S140","AMC/AOC/AVC Ton   R"},
            {"S141","AMC/AOC/AVC Ton   L"},
            {"S142","Tra Ton Schnell R"},
            {"S143","Tra Ton Schnell L"},
            {"S144","Tra Ton Korrekt R"},
            {"S145","Tra Ton Korrekt L"},
            {"S146","Tra Ton Langsam R"},
            {"S147","Tra Ton Langsam L"},
            {"S150","AMC/MOC Taste"},
            {"S160","AMC/MOC Tastenende"},
            {"S170","AVC/VOC Cue   R"},
            {"S171","AVC/VOC Cue   L"},
            {"S180","AVC/VOC Cueende   R"},
            {"S181","AVC/VOC Cueende   L"},
            {"S200","All außer Met Ton Reminder"},
            {"S250","All Leertaste"}
        };



        List<Range> GetRanges()
        {
            List<Range> result = new List<Range>();
            int start = 0; Range r = null;

            while ((r = Log.GetRange(start)) != null)
            {
                if (r.Length > 20) result.Add(r);
                start = r.Next();
            }
            return result;
        }

        public Messung(FileInfo log)
        {
            Proband = log.Name;
            Log = new MarkerFile(log);

            List<Range> ranges = GetRanges();

            for (int i = 0; i < ranges.Count; i++)
            {
                Blocks.Add(new Block(Log.Trials, ranges[i]));
            }

            Log.Trials.RemoveAll(x => Messung.ToBeDeleted.Contains(x.Description));

            foreach (Block b in Blocks) { 
                foreach (KeyValuePair<string, int> s in b.Stats)
                {
                    Program.Form.Log(log.Name + " " + b.TrialType, s.Key, s.Value, s.Key != null && Glossary.ContainsKey(s.Key) ? Glossary[s.Key] : "");
                }
                Program.Form.Log(log.Name + " " + b.TrialType, "Total", b.oldCount,"");
                foreach (KeyValuePair<string, int> s in b.NewStats)
                {
                    Program.Form.Log(log.Name + " " + b.TrialType + " New", s.Key, s.Value, s.Key != null && Glossary.ContainsKey(s.Key) ?Glossary[s.Key]:"");
                }
                Program.Form.Log(log.Name + " " + b.TrialType + " New", "Total", b.newCount, "");
            }

            //Log.Write();
        }

        public void Write() { Log.Write(); }
    }
}
