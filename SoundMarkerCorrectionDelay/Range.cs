﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoundMarkerCorrection
{
    class Range
    {
        public readonly int Start;
        public readonly int Length;
        public readonly int Blockmarker;

        public Range(int start, int stop, int blockmarker)
        {
            Start = start; Length = (stop - start) + 1; Blockmarker = blockmarker;
        }

        public int Next()
        {
            return Start + Length;
        }

        public override string ToString()
        {
            return string.Format("{0}, L{1}", Start, Length);
        }
    }
}
