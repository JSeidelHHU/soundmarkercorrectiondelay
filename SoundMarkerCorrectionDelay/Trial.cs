﻿using System;
using System.Collections.Generic;

namespace SoundMarkerCorrection
{
    class Trial 
    {
        public Line line;
        public string Type, Description;
        public int Position;
        public int Index;
        public int Onset = 0;
        public bool hasBlock = false;
        public bool isBlockmarker = false;
        public bool isStimulus = false;
        public int Marker = 0;
        public int NewMarker = 0;
        public Line newLine;
        public string newDescription = "";

        public Trial(Line line, int index)
        {
            this.line = line;
            if (line.IsMarker)
            {
                Type = line.sValue("Type");
                Description = line.sValue("Description");
                Position = line.iValue("Position");
                Index = index;
                isStimulus = Type == "Stimulus";
                Marker = MarkerValue();
                if (isStimulus && MarkerValue() < 20) isBlockmarker = true;
            }
        }


        public int MarkerValue()
        {
            if (Description.StartsWith("S"))
                return int.Parse(Description.TrimStart('S', ' '));
            else return 0;
        }

        public bool IsMarker { get { return line.IsMarker;  } }

        public Trial (Trial t)
        {
            Type = t.Type;
            Description = t.Description;
            Position = t.Position;
            Index = t.Index;
            Onset = t.Onset;
            line = new Line(t.line);
        }

        public void SetDescription (string d)
        {
            newLine = new Line(line);
            newLine.Replace(Description, d);
            newDescription = d;
        }

        public void SetIndex(int index )
        {
            Index = index;

        }

        Dictionary<string, int> Sort = new Dictionary<string, int> {
            {"Response", 1},
            {"Picture", 2},
            {"Sound", 3},
        };


        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3}, {4}=>{5}", Type, Description, Position, Onset, Marker,NewMarker);
        }
    }
}
